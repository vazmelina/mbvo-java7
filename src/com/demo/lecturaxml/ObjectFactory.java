
package com.demo.lecturaxml;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Transaction }
     * 
     */
    public Transaction createTransaction() {
        return new Transaction();
    }

    /**
     * Create an instance of {@link Transaction.Request }
     * 
     */
    public Transaction.Request createTransactionRequest() {
        return new Transaction.Request();
    }

    /**
     * Create an instance of {@link Transaction.Request.Persons }
     * 
     */
    public Transaction.Request.Persons createTransactionRequestPersons() {
        return new Transaction.Request.Persons();
    }

    /**
     * Create an instance of {@link Transaction.Request.Persons.Person }
     * 
     */
    public Transaction.Request.Persons.Person createTransactionRequestPersonsPerson() {
        return new Transaction.Request.Persons.Person();
    }

    /**
     * Create an instance of {@link Transaction.Request.Persons.Person.Hobbies }
     * 
     */
    public Transaction.Request.Persons.Person.Hobbies createTransactionRequestPersonsPersonHobbies() {
        return new Transaction.Request.Persons.Person.Hobbies();
    }

    /**
     * Create an instance of {@link Transaction.Request.Persons.Person.Contact }
     * 
     */
    public Transaction.Request.Persons.Person.Contact createTransactionRequestPersonsPersonContact() {
        return new Transaction.Request.Persons.Person.Contact();
    }

    /**
     * Create an instance of {@link Transaction.Request.Persons.Person.Name }
     * 
     */
    public Transaction.Request.Persons.Person.Name createTransactionRequestPersonsPersonName() {
        return new Transaction.Request.Persons.Person.Name();
    }

    /**
     * Create an instance of {@link Transaction.Request.Persons.Person.Hobbies.Videogame }
     * 
     */
    public Transaction.Request.Persons.Person.Hobbies.Videogame createTransactionRequestPersonsPersonHobbiesVideogame() {
        return new Transaction.Request.Persons.Person.Hobbies.Videogame();
    }

    /**
     * Create an instance of {@link Transaction.Request.Persons.Person.Hobbies.Trip }
     * 
     */
    public Transaction.Request.Persons.Person.Hobbies.Trip createTransactionRequestPersonsPersonHobbiesTrip() {
        return new Transaction.Request.Persons.Person.Hobbies.Trip();
    }

    /**
     * Create an instance of {@link Transaction.Request.Persons.Person.Hobbies.Movie }
     * 
     */
    public Transaction.Request.Persons.Person.Hobbies.Movie createTransactionRequestPersonsPersonHobbiesMovie() {
        return new Transaction.Request.Persons.Person.Hobbies.Movie();
    }

    /**
     * Create an instance of {@link Transaction.Request.Persons.Person.Contact.Phone }
     * 
     */
    public Transaction.Request.Persons.Person.Contact.Phone createTransactionRequestPersonsPersonContactPhone() {
        return new Transaction.Request.Persons.Person.Contact.Phone();
    }

}
