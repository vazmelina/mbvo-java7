package com.demo.lecturaxml;

public class NoMailException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public NoMailException(String errorMessage) {
		super(errorMessage);
	}
}