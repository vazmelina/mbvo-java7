package com.demo.lecturaxml;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.demo.lecturaxml.Transaction.Request.Persons.Person;
import com.demo.lecturaxml.Transaction.Request.Persons.Person.Hobbies.Movie;
import com.demo.lecturaxml.Transaction.Request.Persons.Person.Hobbies.Trip;
import com.demo.lecturaxml.Transaction.Request.Persons.Person.Hobbies.Videogame;

public class PersonaControl extends Transaction {

	// ---------- Variables
	// -------------------------------------------------------------------------
	private File xmlFile;
	private List<Person> personas;
	List<ConcatenateHobbies> hobbies = new ArrayList<ConcatenateHobbies>();

	// ---------- PersonaControl
	// ---------------------------------------------------------------------
	public PersonaControl() {
		super();
	}

	// ---------- Propiedades
	// -----------------------------------------------------------------------
	public void setPath(String path) {
		xmlFile = new File(path);
	}

	public File getFile() {
		return xmlFile;
	}

	public void listaPersonas_configurar() throws JAXBException {
		JAXBContext jaxbContext;
		jaxbContext = JAXBContext.newInstance(Transaction.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Transaction Persona = (Transaction) jaxbUnmarshaller.unmarshal(this.getFile());
		personas = Persona.getRequest().getPersons().getPerson();
		mail_validar();
	}

	public List<Person> getPersonas() {
		return personas;
	}

	public void listaPersonas_ordenar(Ordenar ordenar) {
		switch (ordenar) {
		case ID:

			Collections.sort(this.getPersonas(), new Comparator<Person>() {

				@Override
				public int compare(Person o1, Person o2) {
					return o1.getId() - (o2.getId());
				}
			});
			break;

		case AGE:
			Collections.sort(this.getPersonas(), new Comparator<Person>() {

				@Override
				public int compare(Person o1, Person o2) {
					return o1.getAge() - (o2.getAge());
				}
			});
			break;
		}
	}

	public void listaHobbies_ordenar(final boolean asc) {
		for (Iterator<Person> persona = this.getPersonas().iterator(); persona.hasNext();) {
			Person person = (Person) persona.next();
			List<Movie> movie = person.getHobbies().getMovie();
			List<Trip> trip = person.getHobbies().getTrip();
			List<Videogame> videogame = person.getHobbies().getVideogame();

			for (Movie movies : movie) {
				ConcatenateHobbies hobbie = new ConcatenateHobbies();
				hobbie.setId(person.getId());
				hobbie.setName(movies.getName().toString().toLowerCase());
				hobbie.setType("movie");
				hobbies.add(hobbie);
			}

			for (Trip trips : trip) {
				ConcatenateHobbies hobbie = new ConcatenateHobbies();
				hobbie.setId(person.getId());
				hobbie.setName(trips.getName().toString().toLowerCase());
				hobbie.setType("trip");
				hobbies.add(hobbie);
			}

			for (Videogame videogames : videogame) {
				ConcatenateHobbies hobbie = new ConcatenateHobbies();
				hobbie.setId(person.getId());
				hobbie.setName(videogames.getName().toString().toLowerCase());
				hobbie.setType("videogames");
				hobbies.add(hobbie);
			}

			Collections.sort(hobbies, new Comparator<ConcatenateHobbies>() {

				@Override
				public int compare(ConcatenateHobbies o1, ConcatenateHobbies o2) {
					int ret;
					if (asc) {
						ret = o1.getName().toUpperCase().compareTo(o2.getName().toUpperCase());
					} else {
						ret = o2.getName().toUpperCase().compareTo(o1.getName().toUpperCase());
					}
					return ret;
				}
			});
		}
	}

	public void hobbies_obtener(short id) {
		for (Iterator<ConcatenateHobbies> hobbie = hobbies.iterator(); hobbie.hasNext();) {
			ConcatenateHobbies concatenatHobbies = (ConcatenateHobbies) hobbie.next();
			if (concatenatHobbies.getId() == id)
				System.out.println(concatenatHobbies.getName() + "->" + concatenatHobbies.getType());
		}
	}

	public void mail_validar() {
		for (Iterator<Person> persona = personas.iterator(); persona.hasNext();) {
			Person person = (Person) persona.next();

			if (person.getContact().getEmail().equals("")) {
				try {
					throw new NoMailException("No mail");
				} catch (NoMailException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

class ConcatenateHobbies {
	private short id;
	private String name;
	private String type;

	public short getId() {
		return id;
	}

	public void setId(short id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
