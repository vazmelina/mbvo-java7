
package com.demo.lecturaxml;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "request"
})
@XmlRootElement(name = "Transaction")
public class Transaction {

    @XmlElement(name = "Request", required = true)
    protected Transaction.Request request;

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link Transaction.Request }
     *     
     */
    public Transaction.Request getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link Transaction.Request }
     *     
     */
    public void setRequest(Transaction.Request value) {
        this.request = value;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "persons"
    })
    public static class Request {

        @XmlElement(name = "Persons", required = true)
        protected Transaction.Request.Persons persons;

        /**
         * Gets the value of the persons property.
         * 
         * @return
         *     possible object is
         *     {@link Transaction.Request.Persons }
         *     
         */
        public Transaction.Request.Persons getPersons() {
            return persons;
        }

        /**
         * Sets the value of the persons property.
         * 
         * @param value
         *     allowed object is
         *     {@link Transaction.Request.Persons }
         *     
         */
        public void setPersons(Transaction.Request.Persons value) {
            this.persons = value;
        }

        
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "person"
        })
        public static class Persons {

            @XmlElement(name = "Person", required = true)
            protected List<Transaction.Request.Persons.Person> person;

            
            public List<Transaction.Request.Persons.Person> getPerson() {
                if (person == null) {
                    person = new ArrayList<Transaction.Request.Persons.Person>();
                }
                return this.person;
            }


            
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "name",
                "contact",
                "hobbies"
            })
            public static class Person {


                @XmlElement(name = "Name", required = true)
                protected Transaction.Request.Persons.Person.Name name;
                @XmlElement(name = "Contact", required = true)
                protected Transaction.Request.Persons.Person.Contact contact;
                @XmlElement(name = "Hobbies", required = true)
                protected Transaction.Request.Persons.Person.Hobbies hobbies;
                @XmlAttribute(name = "id", required = true)
                @XmlSchemaType(name = "unsignedByte")
                protected short id;
                @XmlAttribute(name = "age", required = true)
                @XmlSchemaType(name = "unsignedByte")
                protected short age;

                /**
                 * Gets the value of the name property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Request.Persons.Person.Name }
                 *     
                 */
                public Transaction.Request.Persons.Person.Name getName() {
                    return name;
                }

                /**
                 * Sets the value of the name property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Request.Persons.Person.Name }
                 *     
                 */
                public void setName(Transaction.Request.Persons.Person.Name value) {
                    this.name = value;
                }

                /**
                 * Gets the value of the contact property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Request.Persons.Person.Contact }
                 *     
                 */
                public Transaction.Request.Persons.Person.Contact getContact() {
                    return contact;
                }

                /**
                 * Sets the value of the contact property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Request.Persons.Person.Contact }
                 *     
                 */
                public void setContact(Transaction.Request.Persons.Person.Contact value) {
                    this.contact = value;
                }

                /**
                 * Gets the value of the hobbies property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Request.Persons.Person.Hobbies }
                 *     
                 */
                public Transaction.Request.Persons.Person.Hobbies getHobbies() {
                    return hobbies;
                }

                /**
                 * Sets the value of the hobbies property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Request.Persons.Person.Hobbies }
                 *     
                 */
                public void setHobbies(Transaction.Request.Persons.Person.Hobbies value) {
                    this.hobbies = value;
                }

                /**
                 * Gets the value of the id property.
                 * 
                 */
                public short getId() {
                    return id;
                }

                /**
                 * Sets the value of the id property.
                 * 
                 */
                public void setId(short value) {
                    this.id = value;
                }

                /**
                 * Gets the value of the age property.
                 * 
                 */
                public short getAge() {
                    return age;
                }

                /**
                 * Sets the value of the age property.
                 * 
                 */
                public void setAge(short value) {
                    this.age = value;
                }

                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "email",
                    "phone"
                })
                public static class Contact {

                    @XmlElement(name = "Email", required = true)
                    protected String email;
                    @XmlElement(name = "Phone", required = true)
                    protected Transaction.Request.Persons.Person.Contact.Phone phone;

                    /**
                     * Gets the value of the email property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getEmail() {
                        return email;
                    }

                    /**
                     * Sets the value of the email property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setEmail(String value) {
                        this.email = value;
                    }

                    /**
                     * Gets the value of the phone property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Transaction.Request.Persons.Person.Contact.Phone }
                     *     
                     */
                    public Transaction.Request.Persons.Person.Contact.Phone getPhone() {
                        return phone;
                    }

                    /**
                     * Sets the value of the phone property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Transaction.Request.Persons.Person.Contact.Phone }
                     *     
                     */
                    public void setPhone(Transaction.Request.Persons.Person.Contact.Phone value) {
                        this.phone = value;
                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Phone {

                        @XmlAttribute(name = "number", required = true)
                        @XmlSchemaType(name = "unsignedLong")
                        protected BigInteger number;

                        /**
                         * Gets the value of the number property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getNumber() {
                            return number;
                        }

                        /**
                         * Sets the value of the number property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setNumber(BigInteger value) {
                            this.number = value;
                        }

                    }

                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "videogame",
                    "trip",
                    "movie"
                })
                public static class Hobbies {

                    @XmlElement(name = "Videogame", required = true)
                    protected List<Transaction.Request.Persons.Person.Hobbies.Videogame> videogame;
                    @XmlElement(name = "Trip", required = true)
                    protected List<Transaction.Request.Persons.Person.Hobbies.Trip> trip;
                    @XmlElement(name = "Movie", required = true)
                    protected List<Transaction.Request.Persons.Person.Hobbies.Movie> movie;

                   
                    public List<Transaction.Request.Persons.Person.Hobbies.Videogame> getVideogame() {
                        if (videogame == null) {
                            videogame = new ArrayList<Transaction.Request.Persons.Person.Hobbies.Videogame>();
                        }
                        return this.videogame;
                    }

                    
                    public List<Transaction.Request.Persons.Person.Hobbies.Trip> getTrip() {
                        if (trip == null) {
                            trip = new ArrayList<Transaction.Request.Persons.Person.Hobbies.Trip>();
                        }
                        return this.trip;
                    }

                    
                    public List<Transaction.Request.Persons.Person.Hobbies.Movie> getMovie() {
                        if (movie == null) {
                            movie = new ArrayList<Transaction.Request.Persons.Person.Hobbies.Movie>();
                        }
                        return this.movie;
                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Movie {

                        @XmlAttribute(name = "name", required = true)
                        protected String name;

                        /**
                         * Gets the value of the name property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getName() {
                            return name;
                        }

                        /**
                         * Sets the value of the name property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setName(String value) {
                            this.name = value;
                        }

                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Trip {

                        @XmlAttribute(name = "name", required = true)
                        protected String name;

                        /**
                         * Gets the value of the name property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getName() {
                            return name;
                        }

                        /**
                         * Sets the value of the name property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setName(String value) {
                            this.name = value;
                        }

                    }


                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Videogame {

                        @XmlAttribute(name = "name", required = true)
                        protected String name;

                        /**
                         * Gets the value of the name property.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getName() {
                            return name;
                        }

                        /**
                         * Sets the value of the name property.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setName(String value) {
                            this.name = value;
                        }

                    }
					

                }


                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Name {

                    @XmlAttribute(name = "firstname", required = true)
                    protected String firstname;
                    @XmlAttribute(name = "lastname", required = true)
                    protected String lastname;

                    /**
                     * Gets the value of the firstname property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getFirstname() {
                        return firstname;
                    }

                    /**
                     * Sets the value of the firstname property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFirstname(String value) {
                        this.firstname = value;
                    }

                    /**
                     * Gets the value of the lastname property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLastname() {
                        return lastname;
                    }

                    /**
                     * Sets the value of the lastname property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLastname(String value) {
                        this.lastname = value;
                    }

                }

            }

        }

    }

}
