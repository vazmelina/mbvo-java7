package com.demo.lecturaxml;

import java.util.Iterator;
import javax.xml.bind.JAXBException;

import com.demo.lecturaxml.Transaction.Request.Persons.Person;

public class LectorApp {

	public static void main(String[] args) {

		String path = "F:\\test1.xml";
		int ordenamiento = 0; // 0 si es Asc, 1 si Desc

		PersonaControl personaControl = new PersonaControl();
		personaControl.setPath(path);

		try {
			Ordenar ordenaAge = Ordenar.AGE;
			Ordenar ordenaId = Ordenar.ID;

			personaControl.listaPersonas_configurar();
			personaControl.listaPersonas_ordenar(ordenaAge);
			personaControl.listaHobbies_ordenar(ordenamiento == 0 ? true : false);

			// imprime la lista ordenada
			Iterator<Person> itListaPersona = personaControl.getPersonas().iterator();
			while (itListaPersona.hasNext()) {
				Person elementoLista = (Person) itListaPersona.next();
				System.out.println("-------------------");
				System.out.println("Persona: \n" + elementoLista.getName().getFirstname() + " "
						+ elementoLista.getName().getLastname() + " " + elementoLista.getAge() + "\n");
				System.out.println("Hobbies:");
				personaControl.hobbies_obtener(elementoLista.getId());
			}

			personaControl.listaPersonas_ordenar(ordenaId);

			System.out.println("\nID MAYOR : "
					+ personaControl.getPersonas().get(personaControl.getPersonas().size() - 1).getId() + "\n"
					+ personaControl.getPersonas().get(personaControl.getPersonas().size() - 1).getName().firstname
					+ personaControl.getPersonas().get(personaControl.getPersonas().size() - 1).getName().lastname
					+ "\n" + personaControl.getPersonas().get(personaControl.getPersonas().size() - 1).getAge() + "\n"
					+ personaControl.getPersonas().get(personaControl.getPersonas().size() - 1).getContact().getEmail()
					+ "\n" + personaControl.getPersonas().get(personaControl.getPersonas().size() - 1).getContact()
							.getPhone().number);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
}
