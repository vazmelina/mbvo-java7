package com.demo.generaxml;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.demo.generaxml.Transaction.Request.Persons.Person;

public class PersonaControl extends Transaction {

	// ---------- Variables
	// -------------------------------------------------------------------------
	private File xmlFile;
	private List<Person> personas;

	// ---------- PersonaControl
	// ---------------------------------------------------------------------
	public PersonaControl() {
		super();
	}

	// ---------- Propiedades
	// -----------------------------------------------------------------------
	public void setPath(String path) {
		xmlFile = new File(path);
	}

	public File getFile() {
		return xmlFile;
	}

	public List<Person> getPersonas() {
		return personas;
	}

	public void listaPersonas_configurar() throws JAXBException {
		JAXBContext jaxbContext;
		jaxbContext = JAXBContext.newInstance(Transaction.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Transaction transaction = (Transaction) jaxbUnmarshaller.unmarshal(this.getFile());
		personas = transaction.getRequest().getPersons().getPerson();
	}

	public void listaPersonas_ordenar(final boolean asc) {

		Collections.sort(this.getPersonas(), new Comparator<Person>() {

			@Override
			public int compare(Person o1, Person o2) {
				int ret;
				if (asc) {
					ret = o1.getAge() - (o2.getAge());
				} else {
					ret = o2.getAge() - (o1.getAge());
				}
				return ret;
			}
		});

	}

	public void hilosPersonas_asignar() {
		List<Person> person = this.getPersonas();
		int i = 0;

		for (Person persona : person) {

			System.out.println("Hilo Prioridad Baja Tiene la edad de:" + persona.getAge() + " a�os.");

			ManejadorHilos hilo = new ManejadorHilos(" Numero de Hilo = " + i);
			if (i == 0) {
				System.out.println("Hilo = " + i + " Prioridad Baja Tiene la edad de:" + persona.getAge() + " a�os.");
				hilo.setPriority(Thread.MIN_PRIORITY);
			}
			if (i == person.size() - 1) {
				hilo.setPriority(Thread.MAX_PRIORITY);
				System.out.println("Hilo = " + i + " Prioridad Alta Tiene la edad de: " + persona.getAge() + " a�os.");
			}

			hilo.run(persona.getName().getFirstname(), persona.getName().getLastname(), persona.getId(),
					persona.getAge());
			System.out.println("En " + hilo.getName() + ", el recuento es " + i);
			i++;
			System.out.println("Finalizado.");

		}

	}
}
