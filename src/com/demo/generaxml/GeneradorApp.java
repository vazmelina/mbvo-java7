package com.demo.generaxml;

import javax.xml.bind.JAXBException;

public class GeneradorApp {

	public static void main(String[] args) throws InterruptedException {

		PersonaControl personaControl = new PersonaControl();
		personaControl.setPath("F:\\test1.xml");
		int ordenamiento = 0; // 0 si es Asc, 1 si desc

		try {
			personaControl.listaPersonas_configurar();
			personaControl.listaPersonas_ordenar(ordenamiento == 0 ? true : false);
			personaControl.hilosPersonas_asignar();
		} catch (JAXBException e) {
			
			e.printStackTrace();
		}
	}

}
