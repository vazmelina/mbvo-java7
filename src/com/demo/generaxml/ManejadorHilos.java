package com.demo.generaxml;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class ManejadorHilos extends Thread {
	ManejadorHilos(String nombre){
        super(nombre);
    }
	
public void run(String FirstName, String SecondName,short id, short age){
	try { 
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("Person");
		doc.appendChild(rootElement);

		Element personE = doc.createElement("Persons");
		rootElement.appendChild(personE);
		
		Element firstNameE = doc.createElement("FirstName");
		firstNameE.appendChild(doc.createTextNode(FirstName));
		personE.appendChild(firstNameE);
		
		Element lastNameE = doc.createElement("SecondName");
		lastNameE.appendChild(doc.createTextNode(SecondName));
		personE.appendChild(lastNameE);

		Element idE = doc.createElement("Id");
		idE.appendChild(doc.createTextNode(String. valueOf(id)));
		personE.appendChild(idE);
	
		Element edadE = doc.createElement("Age");
		edadE.appendChild(doc.createTextNode(String. valueOf(age)));
		personE.appendChild(edadE);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
		StreamResult result = new StreamResult(new File("F:\\"+FirstName+"_"+SecondName+"_"+id+"_"+age+".xml"));
		DOMSource source = new DOMSource(doc);
		transformer.transform(source, result);

		System.out.println("Aviso: Se ha creado el XML");

	} catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	} catch (TransformerException tfe) {
		tfe.printStackTrace();
	}
}

}
