package com.demo.generaxml;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "request"
})
@XmlRootElement(name = "Transaction")
public class Transaction {

    @XmlElement(name = "Request", required = true)
    protected Transaction.Request request;

    /**
     * Obtiene el valor de la propiedad request.
     * 
     * @return
     *     possible object is
     *     {@link Transaction.Request }
     *     
     */
    public Transaction.Request getRequest() {
        return request;
    }

    /**
     * Define el valor de la propiedad request.
     * 
     * @param value
     *     allowed object is
     *     {@link Transaction.Request }
     *     
     */
    public void setRequest(Transaction.Request value) {
        this.request = value;
    }
    
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "persons"
    })
    public static class Request {

        @XmlElement(name = "Persons", required = true)
        protected Transaction.Request.Persons persons;

        /**
         * Obtiene el valor de la propiedad persons.
         * 
         * @return
         *     possible object is
         *     {@link Transaction.Request.Persons }
         *     
         */
        public Transaction.Request.Persons getPersons() {
            return persons;
        }

        /**
         * Define el valor de la propiedad persons.
         * 
         * @param value
         *     allowed object is
         *     {@link Transaction.Request.Persons }
         *     
         */
        public void setPersons(Transaction.Request.Persons value) {
            this.persons = value;
        }


        
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "person"
        })
        public static class Persons {

            @XmlElement(name = "Person", required = true)
            protected List<Transaction.Request.Persons.Person> person;

           
            public List<Transaction.Request.Persons.Person> getPerson() {
                if (person == null) {
                    person = new ArrayList<Transaction.Request.Persons.Person>();
                }
                return this.person;
            }


           
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "name",
                "contact",
                "hobbies"
            })
            public static class Person{

                @XmlElement(name = "Name", required = true)
                protected Transaction.Request.Persons.Person.Name name;
                @XmlElement(name = "Contact", required = true)
                protected Transaction.Request.Persons.Person.Contact contact;
                @XmlElement(name = "Hobbies", required = true)
                protected Transaction.Request.Persons.Person.Hobbies hobbies;
                @XmlAttribute(name = "id", required = true)
                @XmlSchemaType(name = "unsignedByte")
                protected short id;
                @XmlAttribute(name = "age", required = true)
                @XmlSchemaType(name = "unsignedByte")
                protected short age;

                /**
                 * Obtiene el valor de la propiedad name.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Request.Persons.Person.Name }
                 *     
                 */
                public Transaction.Request.Persons.Person.Name getName() {
                    return name;
                }

                /**
                 * Define el valor de la propiedad name.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Request.Persons.Person.Name }
                 *     
                 */
                public void setName(Transaction.Request.Persons.Person.Name value) {
                    this.name = value;
                }

                /**
                 * Obtiene el valor de la propiedad contact.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Request.Persons.Person.Contact }
                 *     
                 */
                public Transaction.Request.Persons.Person.Contact getContact() {
                    return contact;
                }

                /**
                 * Define el valor de la propiedad contact.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Request.Persons.Person.Contact }
                 *     
                 */
                public void setContact(Transaction.Request.Persons.Person.Contact value) {
                    this.contact = value;
                }

                /**
                 * Obtiene el valor de la propiedad hobbies.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Transaction.Request.Persons.Person.Hobbies }
                 *     
                 */
                public Transaction.Request.Persons.Person.Hobbies getHobbies() {
                    return hobbies;
                }

                /**
                 * Define el valor de la propiedad hobbies.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Transaction.Request.Persons.Person.Hobbies }
                 *     
                 */
                public void setHobbies(Transaction.Request.Persons.Person.Hobbies value) {
                    this.hobbies = value;
                }

                /**
                 * Obtiene el valor de la propiedad id.
                 * 
                 */
                public short getId() {
                    return id;
                }

                /**
                 * Define el valor de la propiedad id.
                 * 
                 */
                public void setId(short value) {
                    this.id = value;
                }

                /**
                 * Obtiene el valor de la propiedad age.
                 * 
                 */
                public short getAge() {
                    return age;
                }

                /**
                 * Define el valor de la propiedad age.
                 * 
                 */
                public void setAge(short value) {
                    this.age = value;
                }
               
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "email",
                    "phone"
                })
                public static class Contact {

                    @XmlElement(name = "Email", required = true)
                    protected String email;
                    @XmlElement(name = "Phone", required = true)
                    protected Transaction.Request.Persons.Person.Contact.Phone phone;

                    /**
                     * Obtiene el valor de la propiedad email.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getEmail() {
                        return email;
                    }

                    /**
                     * Define el valor de la propiedad email.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setEmail(String value) {
                        this.email = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad phone.
                     * 
                     * @return
                     *     possible object is
                     *     {@link Transaction.Request.Persons.Person.Contact.Phone }
                     *     
                     */
                    public Transaction.Request.Persons.Person.Contact.Phone getPhone() {
                        return phone;
                    }

                    /**
                     * Define el valor de la propiedad phone.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link Transaction.Request.Persons.Person.Contact.Phone }
                     *     
                     */
                    public void setPhone(Transaction.Request.Persons.Person.Contact.Phone value) {
                        this.phone = value;
                    }

                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Phone {

                        @XmlAttribute(name = "number", required = true)
                        @XmlSchemaType(name = "unsignedLong")
                        protected BigInteger number;

                        /**
                         * Obtiene el valor de la propiedad number.
                         * 
                         * @return
                         *     possible object is
                         *     {@link BigInteger }
                         *     
                         */
                        public BigInteger getNumber() {
                            return number;
                        }

                        /**
                         * Define el valor de la propiedad number.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link BigInteger }
                         *     
                         */
                        public void setNumber(BigInteger value) {
                            this.number = value;
                        }

                    }

                }

                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "videogame",
                    "trip",
                    "movie"
                })
                public static class Hobbies {

                    @XmlElement(name = "Videogame", required = true)
                    protected List<Transaction.Request.Persons.Person.Hobbies.Videogame> videogame;
                    @XmlElement(name = "Trip", required = true)
                    protected List<Transaction.Request.Persons.Person.Hobbies.Trip> trip;
                    @XmlElement(name = "Movie", required = true)
                    protected List<Transaction.Request.Persons.Person.Hobbies.Movie> movie;

                    
                    public List<Transaction.Request.Persons.Person.Hobbies.Videogame> getVideogame() {
                        if (videogame == null) {
                            videogame = new ArrayList<Transaction.Request.Persons.Person.Hobbies.Videogame>();
                        }
                        return this.videogame;
                    }

                    public List<Transaction.Request.Persons.Person.Hobbies.Trip> getTrip() {
                        if (trip == null) {
                            trip = new ArrayList<Transaction.Request.Persons.Person.Hobbies.Trip>();
                        }
                        return this.trip;
                    }

                    
                    public List<Transaction.Request.Persons.Person.Hobbies.Movie> getMovie() {
                        if (movie == null) {
                            movie = new ArrayList<Transaction.Request.Persons.Person.Hobbies.Movie>();
                        }
                        return this.movie;
                    }
                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Movie {

                        @XmlAttribute(name = "name", required = true)
                        protected String name;

                        /**
                         * Obtiene el valor de la propiedad name.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getName() {
                            return name;
                        }

                        /**
                         * Define el valor de la propiedad name.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setName(String value) {
                            this.name = value;
                        }

                    }
                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Trip {

                        @XmlAttribute(name = "name", required = true)
                        protected String name;

                        /**
                         * Obtiene el valor de la propiedad name.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getName() {
                            return name;
                        }

                        /**
                         * Define el valor de la propiedad name.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setName(String value) {
                            this.name = value;
                        }

                    }
                    
                    @XmlAccessorType(XmlAccessType.FIELD)
                    @XmlType(name = "")
                    public static class Videogame {

                        @XmlAttribute(name = "name", required = true)
                        protected String name;

                        /**
                         * Obtiene el valor de la propiedad name.
                         * 
                         * @return
                         *     possible object is
                         *     {@link String }
                         *     
                         */
                        public String getName() {
                            return name;
                        }

                        /**
                         * Define el valor de la propiedad name.
                         * 
                         * @param value
                         *     allowed object is
                         *     {@link String }
                         *     
                         */
                        public void setName(String value) {
                            this.name = value;
                        }

                    }

                }
                
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "")
                public static class Name {

                    @XmlAttribute(name = "firstname", required = true)
                    protected String firstname;
                    @XmlAttribute(name = "lastname", required = true)
                    protected String lastname;

                    /**
                     * Obtiene el valor de la propiedad firstname.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getFirstname() {
                        return firstname;
                    }

                    /**
                     * Define el valor de la propiedad firstname.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFirstname(String value) {
                        this.firstname = value;
                    }

                    /**
                     * Obtiene el valor de la propiedad lastname.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLastname() {
                        return lastname;
                    }

                    /**
                     * Define el valor de la propiedad lastname.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLastname(String value) {
                        this.lastname = value;
                    }

                }

            }

        }

    }

}
