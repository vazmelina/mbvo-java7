package com.demo.contador;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeMap;

public class ContadorApp {

	static String [] arreglo = {"casa", "auto", "perro", "avi�n"};
	static String path = "F:\\texto.txt";
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		contarPalabras(leerArchivo(path)); 
	} 
	
	public static String leerArchivo(String path) throws FileNotFoundException, IOException {
		
		String cadena= "";
		String texto = "";
	    FileReader f = new FileReader(path);
	    BufferedReader b = new BufferedReader(f);
	    	while((cadena = b.readLine())!=null) {
	    		texto+=cadena.replace(",","");
	    	}
	    	b.close();
	    return texto;
	}
	
	public static void contarPalabras(String palabra) {
		for(int i=0;i<arreglo.length;i++){
			for(int j=0;j<arreglo.length-1;j++){
				if(i!=j){
					if(arreglo[i] == arreglo[j]){
						arreglo[i] = "";
						}
					}
				}
			}
		
		String[] palabrasArreglo  = palabra.split(" "); 
	    TreeMap<String, Integer> palabraMap = new TreeMap<String, Integer>(); 
	    
	    for (String palabraS : palabrasArreglo) {
	    	for(int n=0;n<arreglo.length;n++){
	            if(palabraS.equals(arreglo[n])){
	            	palabraMap.put(palabraS,(palabraMap.get(palabraS) == null ? 1 : (palabraMap.get(palabraS) + 1))); 
	            }
	    	}
	    }
	    System.out.println(palabraMap);
	   } 
}
